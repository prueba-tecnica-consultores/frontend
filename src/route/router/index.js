import {createBrowserRouter} from "react-router-dom";
import {MainLayout} from "../../views/Layout/Main";
import {
    CharacterDetailScreen,
    CharacterScreen,
    HomeScreen,
    LoginScreen, MovieDetailScreen,
    MoviesScreen,
    RegisterScreen,
    SettingsScreen
} from "../../views";
import {ProtectedRoute} from "../protected";

export const router = createBrowserRouter([
    {
        path: "/",
        element:<MainLayout/>,
        children:[
            { index: true, element: <HomeScreen /> },
            {
                path: "/login",
                element: <LoginScreen/>,

            },
            {
                path: "/register",
                element: <RegisterScreen/>,

            },
            {
                path: "/settings",
                element:<ProtectedRoute><SettingsScreen/></ProtectedRoute>,

            },
            {
                path: "/movies",
                element:<ProtectedRoute><MoviesScreen/></ProtectedRoute>,

            },
            {
                path: "/movies/:id",
                element:<ProtectedRoute><MovieDetailScreen/></ProtectedRoute>,

            },
            {
                path: "/character",
                element:<ProtectedRoute><CharacterScreen/></ProtectedRoute>,

            },
            {
                path: "/character/:id",
                element:<ProtectedRoute><CharacterDetailScreen/></ProtectedRoute>,

            },
        ]

    },

]);
