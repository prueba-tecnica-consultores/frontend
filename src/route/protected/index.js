import {Navigate} from "react-router-dom";
import {useAppSelector} from "../../config/redux";

export const ProtectedRoute = ({ children }) => {
    const {token} = useAppSelector((state)=>state.auth)
    if (!token) {
        return <Navigate to="/" replace />;
    }

    return children;
};
