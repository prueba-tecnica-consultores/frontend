import axios from "axios";
import {trackPromise} from "react-promise-tracker";

export const characterProviders = () => {
    const createCharacterProvider = (data)=>{
        const response =  axios({
            method:'POST',
            url:'/characters',
            data
        })
        return trackPromise(response)
    }
    const getCharactersProvider = ({filter})=>{
        const response = axios({
            method:'GET',
            url:'/characters',
            params:{
                ...filter
            }
        })
        return trackPromise(response)
    }
    const getCharacterProvider = (id)=>{
        const response = axios({
            method:'GET',
            url:`/characters/${id}`,
        })
        return trackPromise(response)
    }
    const deleteCharacterProvider = (id)=>{
        const response =  axios({
            method:'DELETE',
            url:'/characters/'+id,
        })
        return trackPromise(response)
    }
  return {
      createCharacterProvider,
      getCharactersProvider,
      getCharacterProvider,
      deleteCharacterProvider,
  }
}
