import axios from "axios";
import {trackPromise} from "react-promise-tracker";

export const categoryProviders = () => {
    const createCategoryProvider = (data)=>{
        const response =  axios({
            method:'POST',
            url:'/category',
            data
        })
        return trackPromise(response)
    }
    const getCategoriesProvider = ()=>{
        const response =  axios({
            method:'GET',
            url:'/category',
        })
        return trackPromise(response)
    }
    return {
        createCategoryProvider,
        getCategoriesProvider
    }
}
