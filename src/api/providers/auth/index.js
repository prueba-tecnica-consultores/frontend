import axios from "axios";
import {trackPromise} from "react-promise-tracker";

export const  authProviders = ()=>{
    const loginProvider = ({email,password})=> {
        const response =  axios({
            method:'POST',
            url:'/auth',
            data:{
                email,
                password
            }
        })
        return trackPromise(response)
    }
    const registerProvider = ({name,password,email,last_name})=> {
        const response =  axios({
            method:'POST',
            url:'/user',
            data:{name,password,email,last_name}
        })
        return trackPromise(response)
    }
    return {
        loginProvider,
        registerProvider
    }
}
