import axios from "axios";
import {trackPromise} from "react-promise-tracker";

export const movieProviders = () => {
    const createMovieProvider = (data)=>{
        const response =  axios({
            method:'POST',
            url:'/movie',
            data
        })
        return trackPromise(response)
    }
    const getMoviesProvider = ({filter})=>{
        const response =  axios({
            method:'GET',
            url:'/movie',
            params:{
                ...filter,
            }

        })
        return trackPromise(response)
    }
    const getMovieProvider = (id)=>{
        const response =  axios({
            method:'GET',
            url:'/movie/'+id,
        })
        return trackPromise(response)
    }
    const assignCharacterProvider = (data)=>{
        const response =  axios({
            method:'POST',
            url:'/movie/character',
            data
        })
        return trackPromise(response)
    }
    const assignCategoryProvider = (data)=>{
        const response =  axios({
            method:'POST',
            url:'/movie/category',
            data
        })
        return trackPromise(response)
    }
    const deleteMovieProvider = (id)=>{
        const response =  axios({
            method:'DELETE',
            url:'/movie/'+id,
        })
        return trackPromise(response)
    }
    return {
        createMovieProvider,
        getMoviesProvider,
        assignCharacterProvider,
        assignCategoryProvider,
        getMovieProvider,
        deleteMovieProvider
    }
}
