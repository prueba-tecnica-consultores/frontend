import {categoryProviders} from "../../providers";
import {categoryTypes} from "../../../models/types";


export const categoryActions = () => {
    const {createCategoryProvider, getCategoriesProvider} = categoryProviders()
    const {STORE_CATEGORY, GET_CATEGORIES} = categoryTypes()
    const createCategoryAction = (data) => async(dispatch)=> {
        try {
            const response =  await  createCategoryProvider(data);
            dispatch({
                type:STORE_CATEGORY,
                payload:response.data.category
            })
        }catch (e) {
            console.log(e)
        }
    }
    const getCategoriesAction = () => async(dispatch)=> {
        try {
            const {data} =  await  getCategoriesProvider();
            dispatch({
                type:GET_CATEGORIES,
                payload:data.categories
            })
        }catch (e) {
            console.log(e)
        }
    }
    return {
        createCategoryAction,
        getCategoriesAction
    }
}
