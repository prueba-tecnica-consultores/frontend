import {movieProviders} from "../../providers";
import {movieTypes} from "../../../models/types";

export const movieActions = () => {
    const {createMovieProvider, getMoviesProvider,deleteMovieProvider,getMovieProvider, assignCategoryProvider, assignCharacterProvider} = movieProviders()
    const {GET_MOVIES, STORE_MOVIES, GET_MOVIE, DELETE_MOVIE} = movieTypes()
    const createMovieAction = (data) => async(dispatch)=> {
        try {
            const response =  await  createMovieProvider(data);
            dispatch({
                type:STORE_MOVIES,
                payload:response.data.movie
            })
        }catch (e) {
            console.log(e)
        }
    }
    const getMoviesAction = ({filter={}}) => async(dispatch)=> {
        try {
            const {data} =  await  getMoviesProvider({filter});
            dispatch({
                type:GET_MOVIES,
                payload:data.movies
            })
        }catch (e) {
            console.log(e)
        }
    }
    const getMovieAction = (id) => async(dispatch)=> {
        try {
            const {data} =  await  getMovieProvider(id);
            dispatch({
                type:GET_MOVIE,
                payload:data.movie
            })
        }catch (e) {
            console.log(e)
        }
    }
    const deleteMovieAction = (id) => async(dispatch)=> {
        try {
             await  deleteMovieProvider(id);
            dispatch({
                type:DELETE_MOVIE,
                payload:id
            })
        }catch (e) {
            console.log(e)
        }
    }

    const assignCharacterAction = (data) => async(dispatch)=> {
        try {
            const response =  await  assignCharacterProvider(data);
            console.log(response)
        }catch (e) {
            console.log(e)
        }
    }
    const assignCategoryAction = (data) => async(dispatch)=> {
        try {
            const response =  await  assignCategoryProvider(data);
            console.log(response)
        }catch (e) {
            console.log(e)
        }
    }
    return {
        createMovieAction,
        getMoviesAction,
        assignCategoryAction,
        assignCharacterAction,
        getMovieAction,
        deleteMovieAction
    }
}
