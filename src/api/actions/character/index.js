import {characterProviders} from "../../providers";
import {characterTypes} from "../../../models/types";

export const characterActions = () => {
    const {createCharacterProvider, getCharactersProvider, getCharacterProvider, deleteCharacterProvider} = characterProviders()
    const {GET_CHARACTERS, STORE_CHARACTER, GET_CHARACTER, DELETE_CHARACTER} = characterTypes()
    const createCharacterAction = (data) => async(dispatch)=> {
      try {
          const response =  await  createCharacterProvider(data);
          dispatch({
              type:STORE_CHARACTER,
              payload:response.data.character
          })
          console.log(response)
      }catch (e) {
          console.log(e)
      }
    }
    const getCharactersAction = ({filter={}}) => async(dispatch)=> {
        try {
            const {data} =  await  getCharactersProvider({filter});
            dispatch({
                type:GET_CHARACTERS,
                payload:data.characters
            })
        }catch (e) {
            console.log(e)
        }
    }
    const getCharacterAction = (id) => async(dispatch)=> {
        try {
            const {data} =  await  getCharacterProvider(id);
            dispatch({
                type:GET_CHARACTER,
                payload:data.character
            })
        }catch (e) {
            console.log(e)
        }
    }
    const deleteCharacterAction = (id) => async(dispatch)=> {
        try {
            await deleteCharacterProvider(id);
            dispatch({
                type:DELETE_CHARACTER,
                payload:id
            })
        }catch (e) {
            console.log(e)
        }
    }
  return {
      createCharacterAction,
      getCharactersAction,
      getCharacterAction,
      deleteCharacterAction
  }
}
