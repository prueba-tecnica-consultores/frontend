import {authProviders} from "../../providers";
import {authTypes} from "../../../models/types";

export const authActions = () => {
    const {loginProvider, registerProvider} = authProviders()
    const {LOGOUT, LOGIN, REGISTER} = authTypes()
    const loginAction = ({user, onSuccess})=> async (dispatch)=>{
        try {
            const {data} = await loginProvider(user)
            dispatch({
                type:LOGIN,
                payload:data
            })
            onSuccess && onSuccess();
        }catch (e) {
            console.log(e)
        }
    }
    const registerAction = ({user, onSuccess})=>async (dispatch)=>{
        try {
            const {data} = await registerProvider(user)
            dispatch({
                type:REGISTER,
                payload:data
            })
            onSuccess && onSuccess();
        }catch (e) {
            console.log(e)
        }
    }
    const logoutAction = ()=>(dispatch)=>{
        dispatch({
            type:LOGOUT
        })
    }
  return {
      loginAction,
      registerAction,
      logoutAction
  }
}
