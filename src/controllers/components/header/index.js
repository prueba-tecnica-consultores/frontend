import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {authActions} from "../../../api/actions/auth";
import {useNavigate} from "react-router-dom";

export const useHeader = () => {

  const {user} =  useAppSelector((state)=>state.auth)
  const dispatch = useAppDispatch()
  const navigate =  useNavigate()
  const {logoutAction} = authActions()
  const logout = ()=>{
    dispatch(logoutAction())
    toRoute('/')
  }
  const toRoute = (page) =>{
    navigate(`/${page}`)
  }

  const pages = [ {title:'Peliculas y Series', onClick:()=>toRoute('movies')}, {title:'Personajes', onClick:()=>toRoute('character')}];
  const settings = [{title:'Ajustes', onClick: ()=>toRoute('settings') }, {title:'Salir', onClick:logout}];

  return {
    user,
    settings,
    pages,
    toRoute
  }
}
