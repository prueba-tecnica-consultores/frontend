import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {characterActions, movieActions} from "../../../api";
import {useEffect, useState} from "react";

export const useCharacter = () => {
    const [filter,setFilter] = useState({})
    const dispatch = useAppDispatch()
    const { getCharactersAction}= characterActions()
    const { getMoviesAction} = movieActions()
    const {characters, movies} = useAppSelector((state)=>state)
    useEffect(()=>{
        if(characters.data.length<1){
            dispatch(getCharactersAction({}))
        }
        if(movies.data.length<1){
            dispatch(getMoviesAction({}))
        }
    },[])
    const handleChange =(e)=>{
        setFilter({
            ...filter,
            [e.target.name]:e.target.value
        })
    }

    const getCharacters = ()=>{
        dispatch(getCharactersAction({filter}))
    }
  return {
        characters,
        movies,
      handleChange,
      getCharacters,
  }
}
