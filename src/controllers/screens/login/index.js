import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {useNavigate} from "react-router-dom";
import {authActions} from "../../../api/actions/auth";
import {useAppDispatch} from "../../../config/redux";
import {useSelector} from "react-redux";
import {useEffect} from "react";
import * as yup from "yup";

const loginSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().min(5).max(10).required(),
});
export const useLogin = () => {
    const {
        register,
        handleSubmit,
        reset
    } = useForm({
        resolver: yupResolver(loginSchema),
    });
    const navigate = useNavigate()
    const {loginAction} = authActions()
    const dispatch = useAppDispatch()
    const auth = useSelector((state)=>state.auth)
    console.log(auth)
    useEffect(()=>{
        if(auth.token){
            navigate('/')
        }
    }, [auth.token])
    const onSubmit = (user)=>{
        console.log({user})
        dispatch(loginAction({user}))
        reset();
    }
    return {
        register,
        handleSubmit,
        onSubmit
    }
}
