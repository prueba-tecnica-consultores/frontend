import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {categoryActions, movieActions} from "../../../api";
import {useEffect, useState} from "react";

export const useMovies = () => {
    const [filter,setFilter] = useState({})
    const dispatch = useAppDispatch()
    const { getMoviesAction} = movieActions()
    const {getCategoriesAction} = categoryActions()
    const {movies, category} = useAppSelector((state)=>state)
    useEffect(()=>{
        if(movies.data.length<1){
            dispatch(getMoviesAction({}))
        }
        if(category.data.length<1){
            dispatch(getCategoriesAction())
        }
    },[])
    const handleChange =(e)=>{
        setFilter({
            ...filter,
            [e.target.name]:e.target.value
        })
    }

    const getMovies = ()=>{
        dispatch(getMoviesAction({filter}))
    }
    return {
        movies,
        categories:category,
        handleChange,
        getMovies,
    }
}
