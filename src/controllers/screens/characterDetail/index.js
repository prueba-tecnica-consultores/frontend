import {useParams} from "react-router-dom";
import {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {characterActions} from "../../../api";

export const useCharacterDetail = () => {
    const { id } = useParams();
    const {item} = useAppSelector(state => state.characters)
    const dispatch = useAppDispatch()
    const {getCharacterAction} = characterActions()
    useEffect(()=>{
        dispatch(getCharacterAction(id))
    },[])
  return {
    item
  }
}
