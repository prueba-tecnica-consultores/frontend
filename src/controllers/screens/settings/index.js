import * as yup from "yup";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {convertToFormData} from 'buildformdata-rmonterrosa'
import {useAppDispatch, useAppSelector} from "../../../config/redux";
import {categoryActions, characterActions, movieActions} from "../../../api";
import {useEffect, useState} from "react";

const characterSchema = yup.object().shape({
    name:yup.string().required(),
    history:yup.string().required(),
    age:yup.number().required(),
    weight:yup.number().required(),
    image: yup.mixed().required(),
});
const movieSchema = yup.object().shape({
    title:yup.string().required(),
    rate:yup.number().required(),
    date:yup.date().required(),
    image: yup.mixed().required(),
});
const categorySchema = yup.object().shape({
    name:yup.string().required(),
    image: yup.mixed().required(),
});
const movieCategorySchema = yup.object().shape({
    movie_id:yup.number().required(),
    category_id:yup.number().required(),
});
const movieCharacterSchema = yup.object().shape({
    movie_id:yup.number().required(),
    character_id:yup.number().required(),
});
export const useSettings = () => {
    const characterForm= useForm({
        resolver: yupResolver(characterSchema),
    });
    const movieForm= useForm({
        resolver: yupResolver(movieSchema),
    });
    const categoryForm= useForm({
        resolver: yupResolver(categorySchema),
    });
    const movieCharacterForm= useForm({
        resolver: yupResolver(movieCharacterSchema),
    });
    const movieCategoryForm= useForm({
        resolver: yupResolver(movieCategorySchema),
    });
    const dispatch = useAppDispatch()
    const {createCharacterAction, getCharactersAction, deleteCharacterAction}= characterActions()
    const {createMovieAction, getMoviesAction, assignCategoryAction, assignCharacterAction, deleteMovieAction} = movieActions()
    const {createCategoryAction, getCategoriesAction} = categoryActions()
    const {movies,characters, category} = useAppSelector((state)=>state)
    const [deleteItem,setDelete] = useState({})
    useEffect(()=>{
        dispatch(getCharactersAction({}))
        dispatch(getMoviesAction({}))
        dispatch(getCategoriesAction())
    },[])
    const saveCharacter = (data)=>{
        const character = convertToFormData({...data, image:data.image[0]})
        dispatch(createCharacterAction(character))
    }
    const saveMovie = (data)=>{
        const movie = convertToFormData({...data, image:data.image[0], date:new Date(data.date).toISOString()})
        dispatch(createMovieAction(movie))
    }
    const saveCategory = (data)=>{
        const category = convertToFormData({...data, image:data.image[0]})
        dispatch(createCategoryAction(category))
    }
    const saveMovieCharacter = (data)=>{
        dispatch(assignCharacterAction(data))
    }
    const saveMovieCategory = (data)=>{
        dispatch(assignCategoryAction(data))
    }
    const deleteMovie = () =>{
        dispatch(deleteMovieAction(deleteItem.movie_id))
    }
    const deleteCharacter = ()=>{
        dispatch(deleteCharacterAction(deleteItem.character_id))
    }
    const handleChange = (e)=>{
        setDelete({
            [e.target.name]:e.target.value
        })
    }
  return {
      characterForm,
      movieForm,
      categoryForm,
      movieCharacterForm,
      movieCategoryForm,
      saveCharacter,
      saveMovie,
      saveCategory,
      saveMovieCharacter,
      saveMovieCategory,
      deleteMovie,
      deleteCharacter,
      handleChange,
      movies,
      characters,
      category
  }
}
