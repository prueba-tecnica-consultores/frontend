import {useParams} from "react-router-dom";
import {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../../config/redux";
import  {movieActions} from "../../../api";

export const useMovieDetail = () => {
    const { id } = useParams();
    const {item} = useAppSelector(state => state.movies)
    const dispatch = useAppDispatch()
    const {getMovieAction} = movieActions()
    useEffect(()=>{
        dispatch(getMovieAction(id))
    },[])
    return {
        item
    }
}
