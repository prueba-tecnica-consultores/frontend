import {useForm} from "react-hook-form";
import * as yup from "yup";
import {yupResolver} from "@hookform/resolvers/yup";
import {authActions} from "../../../api/actions/auth";
import {useAppDispatch} from "../../../config/redux";
import {useNavigate} from "react-router-dom";

const registerSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().min(6).max(8).required(),
    name:yup.string().required(),
    last_name:yup.string().required(),
});
export const useRegister = () => {
    const {
        register,
        handleSubmit,
        reset
    } = useForm({
        resolver: yupResolver(registerSchema),
    });
    const navigate = useNavigate()
    const {registerAction} = authActions()
    const dispatch = useAppDispatch()

    const onSubmit = (user)=>{
        console.log({user})
        dispatch(registerAction({user, onSuccess:()=>navigate('/')}))
        reset()
    }
    return {
        register,
        handleSubmit,
        onSubmit
    }
}
