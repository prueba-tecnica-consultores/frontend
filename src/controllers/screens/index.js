export {useMovieDetail} from "./movieDetail";
export {useCharacterDetail} from "./characterDetail";
export {useMovies} from "./movie";
export {useCharacter} from "./character";
export {useLogin} from "./login";
export {useSettings} from "./settings";
export {useRegister} from "./register";


