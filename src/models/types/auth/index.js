export const authTypes = ()=>{
    const LOGIN = 'LOGIN'
    const LOGOUT = 'LOGOUT'
    const REGISTER = 'REGISTER'
    return {
        LOGIN,
        LOGOUT,
        REGISTER,
    }
}
