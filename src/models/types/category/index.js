export const categoryTypes = ()=>{
    const GET_CATEGORY = 'GET_CATEGORY'
    const GET_CATEGORIES = 'GET_CATEGORIES'
    const STORE_CATEGORY = 'STORE_CATEGORY'
    const DELETE_CATEGORY = 'DELETE_CATEGORY'

    return {
        GET_CATEGORY,
        GET_CATEGORIES,
        STORE_CATEGORY,
        DELETE_CATEGORY
    }
}
