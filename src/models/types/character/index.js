export const characterTypes = ()=>{
    const GET_CHARACTER = 'GET_CHARACTER'
    const GET_CHARACTERS = 'GET_CHARACTERS'
    const STORE_CHARACTER = 'STORE_CHARACTER'
    const DELETE_CHARACTER = 'DELETE_CHARACTER'

    return {
        GET_CHARACTER,
        GET_CHARACTERS,
        STORE_CHARACTER,
        DELETE_CHARACTER
    }
}
