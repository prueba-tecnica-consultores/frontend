export const movieTypes = ()=>{
    const GET_MOVIE = 'GET_MOVIE'
    const GET_MOVIES = 'GET_MOVIES'
    const STORE_MOVIES = 'STORE_MOVIES'
    const DELETE_MOVIE = 'DELETE_MOVIE'

    return {
        GET_MOVIE,
        GET_MOVIES,
        STORE_MOVIES,
        DELETE_MOVIE
    }
}
