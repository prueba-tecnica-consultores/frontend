
export const handleInitialStates = () => {
    const movies = {
        data:[],
    }
    const characters = {
        data:[],
    }
    const category = {
        data:[],
    }
    const auth = {
        token:null,
        user:{

        }
    }
  return {
      movies,
      characters,
      auth,
      category
  }
}
