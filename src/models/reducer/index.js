import {combineReducers} from "redux";
import {handleCharacterReducer} from "./character";
import {handleAuthReducer} from "./auth";
import {handleMovieReducer} from "./movies";
import {handleCategoryReducer} from "./category";

export const handleReducer = ()=>{
    const characters = handleCharacterReducer()
    const auth = handleAuthReducer()
    const movies = handleMovieReducer()
    const category = handleCategoryReducer()
    return combineReducers({
        characters,
        auth,
        movies,
        category
    })
}
