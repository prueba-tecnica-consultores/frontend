import {createReducer} from "@reduxjs/toolkit";
import {handleInitialStates} from "../../initialStates";
import {categoryTypes} from "../../types";

export const handleCategoryReducer = () => {
    const {  GET_CATEGORIES, GET_CATEGORY, STORE_CATEGORY} = categoryTypes()
    const {category} = handleInitialStates()
    const builderCallBack = (builder)=>{
        builder
            .addCase(GET_CATEGORIES,(state, action)=>{
                return {
                    ...state,
                    data:action.payload
                }
            })
            .addCase(GET_CATEGORY,(state, action)=>{
                return {
                    ...state,
                    item: {...state.item,...action.payload}
                }
            })
            .addCase(STORE_CATEGORY,(state, action)=>{
                return {
                    ...state,
                    data:[
                        ...state.data,
                        action.payload
                    ]
                }
            })
    }
    return createReducer(category,builderCallBack)
}
