import {createReducer} from "@reduxjs/toolkit";
import {authTypes} from "../../types";

import {handleInitialStates} from "../../initialStates";

export const handleAuthReducer = () => {
    const {LOGIN, LOGOUT, REGISTER} = authTypes()
    const {auth} = handleInitialStates()
    const builderCallBack = (builder)=>{
        builder
            .addCase(LOGIN,(state, action)=>{
                return {
                    ...state,
                    ...action.payload
                }
            })
            .addCase(REGISTER,(state, action)=>{
                return {
                    ...state,
                    ...action.payload
                }
            })
            .addCase(LOGOUT,(state, action)=>{
                return auth
            })
    }
    return createReducer(auth,builderCallBack)
}
