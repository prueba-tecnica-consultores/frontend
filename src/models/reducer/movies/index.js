import {createReducer} from "@reduxjs/toolkit";
import {handleInitialStates} from "../../initialStates";
import {movieTypes} from "../../types";

export const handleMovieReducer = () => {
    const { GET_MOVIE, STORE_MOVIES, GET_MOVIES, DELETE_MOVIE} = movieTypes()
    const {movies} = handleInitialStates()
    const builderCallBack = (builder)=>{
        builder
            .addCase(GET_MOVIES,(state, action)=>{
                return {
                    ...state,
                    data:action.payload
                }
            })
            .addCase(GET_MOVIE,(state, action)=>{
                return {
                    ...state,
                    item: action.payload,
                }
            })
            .addCase(STORE_MOVIES,(state, action)=>{
                return {
                    ...state,
                    data:[
                        ...state.data,
                        action.payload
                    ]
                }
            })
            .addCase(DELETE_MOVIE,(state, action)=>{
                return {
                    ...state,
                    data:state.data.filter(d=>d.i!=action.payload)
                }
            })
    }
    return createReducer(movies,builderCallBack)
}
