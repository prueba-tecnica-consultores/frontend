import {createReducer} from "@reduxjs/toolkit";
import {characterTypes} from "../../types";

import {handleInitialStates} from "../../initialStates";

export const handleCharacterReducer = () => {
    const {GET_CHARACTERS, GET_CHARACTER, STORE_CHARACTER, DELETE_CHARACTER} = characterTypes()
    const {characters} = handleInitialStates()
    const builderCallBack = (builder)=>{
        builder
            .addCase(GET_CHARACTERS,(state, action)=>{
                return {
                    ...state,
                    data:action.payload
                }
            })
            .addCase(GET_CHARACTER,(state, action)=>{
                return {
                    ...state,
                    item: action.payload
                }
            })
            .addCase(STORE_CHARACTER,(state, action)=>{
                return {
                    ...state,
                    data:[...state.data, action.payload]
                }
            })
            .addCase(DELETE_CHARACTER,(state, action)=>{
            return {
                ...state,
                data:state.data.filter(d=>d.id!=action.payload)
            }
        })
    }
    return createReducer(characters,builderCallBack)
}
