import * as React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";

export const FooterComponent = () => {
    return (
        <Box
            component="footer"
            sx={{
                backgroundColor:'#0E0B13'
            }}
            style={{
                position:'fixed',
                bottom: '0px',
                left: '0px',
                right: '0px',
            }}
        >
            <Container maxWidth="sm" align="center" >
                <img
                    style={{
                        minWidth: '79px',
                        height: '48px',
                        marginTop:'25px',
                    }}
                    src={'https://static-assets.bamgrid.com/product/disneyplus/images/logo.1a56f51c764022ee769c91d894d44326.svg'}
                    alt={'logo'}
                />
                <div style={{
                    display:'flex',
                    justifyContent:'center',
                    alignItems:'center',
                    gap:25,
                    fontSize:12,
                    marginBottom:25

                }}>
                    <Typography variant="body2" color="white">
                        {"Politica de privacidad"}
                    </Typography>
                    <Typography variant="body2" color="white">
                        {"Complemento Politica de privacidad"}
                    </Typography>
                </div>

            </Container>
        </Box>
    )
}
