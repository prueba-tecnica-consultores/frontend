import Container from "@mui/material/Container";
import {InputBase, InputLabel, Select} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import MenuItem from "@mui/material/MenuItem";
import {CardComponent} from "../../components";
import {useMovies} from "../../../controllers";
import {useNavigate} from "react-router-dom";

export const MoviesScreen = () => {
    const {movies, handleChange, getMovies, categories} = useMovies()
    const navigate = useNavigate()
  return(
      <Container>
        <div style={{
          marginTop:20,
          width:'100%',
          display:'flex',
          justifyContent:'right'
        }}>
          <InputBase
              sx={{ ml: 1, flex: 1, color:'#fff'}}
              placeholder="Busca Personajes"
              inputProps={{ 'aria-label': 'busca personajes' }}
              name='title'
              onChange={handleChange}
          />
          <IconButton type="button" sx={{ p: '10px', color:'#fff' }} aria-label="search" onClick={getMovies} >
            <SearchIcon />
          </IconButton>
        </div>
        <div style={{
          display:"flex",
          gap:20,
          justifyContent:'center',
          alignItems:'center',
        }}>
          <h3>Filtros:</h3>
            <div>
                <InputLabel id="movie-id-label">Generos</InputLabel>
                <Select
                    labelId="movie-id-label"
                    id="category_id"
                    label="Genero"
                    name={'category_id'}
                    onChange={handleChange}
                >
                    {categories.data.map((category, index)=> <MenuItem key={index+category.name} value={category.id}>{category.name}</MenuItem>)}
                </Select>
            </div>
            <div>
                <InputLabel id="movie-id-label">Ordenar por fechas</InputLabel>
                <Select
                    labelId="movie-id-label"
                    id="order"
                    label="Ordenar por fecha"
                    name={'order'}
                    onChange={handleChange}
                >
                    <MenuItem value='ASC'>ASC</MenuItem>
                    <MenuItem value='DESC'>DESC</MenuItem>
                </Select>
            </div>
          <div>
          </div>
        </div>
        <div style={{
          display:'flex',
          gap:20,
          marginTop:50,
        }}>
          {movies.data.map((movie)=>{
            return (
                <CardComponent title={movie.title} description={movie.date} image={`http://localhost:4000/api/images/${movie.image}`} onClick={()=>navigate('/movies/'+movie.id)}/>
            )
          })}
        </div>
      </Container>

  )
}
