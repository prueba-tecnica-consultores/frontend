import Container from "@mui/material/Container";
import SearchIcon from '@mui/icons-material/Search';
import {useCharacter} from "../../../controllers";
import {InputBase, InputLabel, Select, TextField} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import {CardComponent} from "../../components";
import {useNavigate} from "react-router-dom";

export const CharacterScreen = () => {
    const { characters, movies, handleChange, getCharacters} = useCharacter()
    const navigate = useNavigate()
  return(
      <Container>
          <div style={{
              marginTop:20,
              width:'100%',
              display:'flex',
              justifyContent:'right'
          }}>
              <InputBase
                  sx={{ ml: 1, flex: 1, color:'#fff'}}
                  placeholder="Busca Personajes"
                  inputProps={{ 'aria-label': 'busca personajes' }}
                  name='name'
                  onChange={handleChange}
              />
              <IconButton type="button" sx={{ p: '10px', color:'#fff' }} aria-label="search" onClick={getCharacters} >
                  <SearchIcon />
              </IconButton>
          </div>
          <div style={{
              display:"flex",
              gap:20,
              justifyContent:'center',
              alignItems:'center',
          }}>
              <h3>Filtros:</h3>
              <TextField
                  id="age"
                  label="Edad"
                  variant="outlined"
                  sx={{
                      backgroundColor:'#30333D',
                      color:'white',
                  }}
                  name='age'
                  onChange={handleChange}
                  type='number'
              />
              <TextField
                  id="weight"
                  label="Peso"
                  variant="outlined"
                  sx={{
                      backgroundColor:'#30333D',
                      color:'white',
                  }}
                  type='number'
                  name='weight'
                  onChange={handleChange}
              />
              <div>


              <InputLabel id="movie-id-label">Peliculas</InputLabel>
              <Select
                  labelId="movie-id-label"
                  id="movie_id"
                  label="Peliculas"
                  name={'movie_id'}
                  onChange={handleChange}
              >
                  {movies.data.map((movie, index)=> <MenuItem key={index+movie.title} value={movie.id}>{movie.title}</MenuItem>)}
              </Select>
              </div>
          </div>
          <div style={{
              display:'flex',
              gap:20,
              marginTop:50,
          }}>
          {characters.data.map((character)=>{
              return (
                  <CardComponent title={character.name} description={character.history} image={`http://localhost:4000/api/images/${character.image}`} onClick={()=>navigate('/character/'+character.id)}/>
              )
          })}
          </div>
      </Container>

  )
}
