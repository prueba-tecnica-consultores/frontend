import {Grid, InputLabel, Select, TextField} from "@mui/material";
import {AccordionComponent} from "../../components";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import {useSettings} from "../../../controllers";
import MenuItem from "@mui/material/MenuItem";

export const SettingsScreen = () => {
    const {characterForm, movieForm, categoryForm, movieCharacterForm,movieCategoryForm, saveMovie,saveCharacter, saveCategory, saveMovieCharacter, saveMovieCategory,handleChange,deleteCharacter, deleteMovie, movies, characters, category} = useSettings()
  return (
      <Grid container spacing={2}>
          <Grid item xs={6}>
              <AccordionComponent title=' Formulario Personajes'>
                  <Container>
                      <div>
                          <TextField
                              id="image"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='file'
                              {...characterForm.register('image')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="name"
                              label="Nombre"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              {...characterForm.register('name')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="age"
                              label="Edad"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='number'
                              min={1}
                              {...characterForm.register('age')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="weight"
                              label="Peso"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='number'
                              {...characterForm.register('weight')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="history"
                              label="Historia"
                              multiline
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              rows={5}
                              {...characterForm.register('history')}
                          />
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={characterForm.handleSubmit(saveCharacter)}
                          >Guardar Personaje</Button>
                      </div>
                  </Container>
              </AccordionComponent>

          </Grid>
          <Grid item xs={6}>
              <AccordionComponent title='Formulario Peliculas'>
                  <Container>
                      <div>
                          <TextField
                              id="image"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='file'
                              {...movieForm.register('image')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="title"
                              label="Nombre"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              {...movieForm.register('title')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="rate"
                              label="Calificacion (1 a 5)"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='number'
                              max={5}
                              min={1}
                              {...movieForm.register('rate')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="date"
                              label="Fecha"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='date'
                              {...movieForm.register('date')}
                          />
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={movieForm.handleSubmit(saveMovie)}
                          >Guardar Pelicula</Button>
                      </div>
                  </Container>
              </AccordionComponent>
          </Grid>
          <Grid item xs={6}>
              <AccordionComponent title='Formulario Categorias'>
                  <Container>
                      <div>
                          <TextField
                              id="image"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              type='file'
                              {...categoryForm.register('image')}
                          />
                      </div>
                      <div>
                          <TextField
                              id="name"
                              label="Nombre"
                              variant="outlined"
                              sx={{
                                  backgroundColor:'#30333D',
                                  color:'white',
                                  width:367,
                                  marginTop:'2rem'
                              }}
                              {...categoryForm.register('name')}
                          />
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={categoryForm.handleSubmit(saveCategory)}
                          >Guardar Categoria</Button>
                      </div>
                  </Container>
              </AccordionComponent>
          </Grid>
          <Grid item xs={6}>
              <AccordionComponent title='Asignar Personajes Y Peliculas'>
                  <Container>
                      <div style={{
                          flex:1
                      }}>
                          <InputLabel id="character-id-label">Personajes</InputLabel>
                          <Select
                              labelId="character-id-label"
                              id="character_id"
                              label="Personaje"
                              style={{
                                  flex:1
                              }}
                              {...movieCharacterForm.register('character_id')}
                          >
                              {characters.data.map((character, index)=> <MenuItem key={index+character.name} value={character.id}>{character.name}</MenuItem>)}
                          </Select>
                      </div>
                      <div style={{
                          flex:1
                      }}>
                          <InputLabel id="movie-id-label">Peliculas</InputLabel>
                          <Select
                              labelId="movie-id-label"
                              id="movie_id"
                              label="Peliculas"
                              {...movieCharacterForm.register('movie_id')}
                          >
                              {movies.data.map((movie, index)=> <MenuItem key={index+movie.title} value={movie.id}>{movie.title}</MenuItem>)}
                          </Select>
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={movieCharacterForm.handleSubmit(saveMovieCharacter)}
                          >Asignar</Button>
                      </div>
                  </Container>
              </AccordionComponent>
          </Grid>
          <Grid item xs={6}>
              <AccordionComponent title=' Asignar Categorias Y Peliculas'>
                  <Container>
                      <div style={{
                          flex:1
                      }}>
                          <InputLabel id="category-id-label">Categorias</InputLabel>
                          <Select
                              labelId="category-id-label"
                              id="category_id"
                              label="Categoria"
                              style={{
                                  flex:1
                              }}
                              {...movieCategoryForm.register('category_id')}
                          >
                              {category.data.map((character, index)=> <MenuItem key={index+character.name} value={character.id}>{character.name}</MenuItem>)}
                          </Select>
                      </div>
                      <div style={{
                          flex:1
                      }}>
                          <InputLabel id="movie-id-label">Peliculas</InputLabel>
                          <Select
                              labelId="movie-id-label"
                              id="movie_id"
                              label="Peliculas"
                              {...movieCategoryForm.register('movie_id')}
                          >
                              {movies.data.map((movie, index)=> <MenuItem key={index+movie.title} value={movie.id}>{movie.title}</MenuItem>)}
                          </Select>
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={movieCategoryForm.handleSubmit(saveMovieCategory)}
                          >Asignar</Button>
                      </div>
                  </Container>
              </AccordionComponent>

          </Grid>
          <Grid item xs={6}>
              <AccordionComponent title=' Eliminar Personajes Y Peliculas'>
                  <Container>
                      <div style={{
                          flex:1
                      }}>
                          <InputLabel id="character-id-label">Personajes</InputLabel>
                          <Select
                              labelId="character-id-label"
                              id="character_id"
                              name="character_id"
                              label="Personaje"
                              style={{
                                  flex:1
                              }}
                              onChange={handleChange}
                          >
                              {characters.data.map((character, index)=> <MenuItem key={index+character.name} value={character.id}>{character.name}</MenuItem>)}
                          </Select>
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={deleteCharacter}
                          >Eliminar</Button>
                      </div>
                      <div style={{
                          flex:1
                      }}>
                          <InputLabel id="movie-id-label">Peliculas</InputLabel>
                          <Select
                              labelId="movie-id-label"
                              id="movie_id"
                              label="Peliculas"
                              name={'movie_id'}
                              onChange={handleChange}
                          >
                              {movies.data.map((movie, index)=> <MenuItem key={index+movie.title} value={movie.id}>{movie.title}</MenuItem>)}
                          </Select>
                      </div>
                      <div>
                          <Button
                              variant="contained"
                              sx={{
                                  backgroundColor:'#2C73D5',
                                  color:'white',
                                  width:367,
                                  height:48,
                                  marginTop:'2rem'
                              }}
                              onClick={deleteMovie}
                          >Eliminar</Button>
                      </div>
                  </Container>
              </AccordionComponent>

          </Grid>
      </Grid>
  )
}
