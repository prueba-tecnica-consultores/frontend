export {MovieDetailScreen} from "./MovieDetail";
export {CharacterDetailScreen} from "./CharacterDetail";
export {CharacterScreen} from "./Character";
export {MoviesScreen} from "./Movies";
export {HomeScreen} from "./Home";
export {SettingsScreen} from "./Settings";
export {LoginScreen} from "./Login";
export {RegisterScreen} from "./Register";





