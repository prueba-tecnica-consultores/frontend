import {useCharacterDetail} from "../../../controllers";
import Container from "@mui/material/Container";
import {CardComponent} from "../../components";
import {useNavigate} from "react-router-dom";

export const CharacterDetailScreen = () => {
  const {item} = useCharacterDetail();
  const navigate = useNavigate()
    if(!item?.name){
        return <h1>Cargando</h1>
    }
  return (
      <Container style={{
          display:'flex'
      }}>
          <div>
        <h2>{item.name}</h2>
        <img style={{
            width:200,
        }} src={`http://localhost:4000/api/images/${item.image}`} alt={'character'}/>
          <h3>Age:{item.age} Años</h3>
          <h3>Peso:{item.weight} Kg</h3>
          <p>Historia: {item.history}</p>
          </div>
          <h3>Peliculas asociadas</h3>
            <div style={{
                display:'flex',
                gap:20,
                marginTop:50,
            }}>
                {item.Movies.map((movie)=>{
                    return (
                        <CardComponent title={movie.title} description={movie.date} image={`http://localhost:4000/api/images/${movie.image}`} onClick={()=>navigate('/movies/'+movie.id)}/>
                    )
                })}
            </div>


      </Container>
  )
}
