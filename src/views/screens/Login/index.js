import {TextField} from "@mui/material";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import {useLogin} from "../../../controllers";
import {Link} from "react-router-dom";

export const LoginScreen = () => {
    const { onSubmit, handleSubmit, register} = useLogin()
  return  (
      <Container
          maxWidth="sm" align="center"
          style={{
              color:'white',
          }}>
        <div style={{
            maxWidth:367,
            textAlign:'justify',
        }}>
            <div style={{
                fontSize:24,
            }}> <h3>Inicia sesión con tu correo</h3></div>
            <div style={{
                fontSize:15
            }}> <span>Debes usar este correo electrónico y contraseña para iniciar sesión en tu cuenta de Disney+ y ver tus series y películas favoritas.</span></div>
            <div>
                <TextField
                    id="email"
                    label="Correo electronico"
                    variant="outlined"
                    sx={{
                        backgroundColor:'#30333D',
                        color:'white',
                        width:367,
                        marginTop:'2rem'
                    }}
                    {...register('email')}
                />
            </div>
            <div>
                <TextField
                    id="password"
                    type="password"
                    label="Contraseña"
                    variant="outlined"
                    sx={{
                        backgroundColor:'#30333D',
                        color:'white',
                        width:367,
                        marginTop:'2rem'
                    }}
                    {...register('password')}
                />
            </div>
            <div>
                <Button variant="contained" sx={{
                    backgroundColor:'#2C73D5',
                    color:'white',
                    width:367,
                    height:48,
                    marginTop:'2rem'
                }} onClick={handleSubmit(onSubmit)}>Ingresar</Button>
            </div>
            <div style={{
                fontSize:15,
                display:"flex",
                gap:10,
                marginTop:20,
                alignItems:'center'
            }}>
                <p>primera vez en disney?</p> <Link to={'/register'}>Suscribirse</Link>
            </div>
        </div>
      </Container>
  )
}
