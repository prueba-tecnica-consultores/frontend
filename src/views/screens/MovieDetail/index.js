import { useMovieDetail} from "../../../controllers";
import {useNavigate} from "react-router-dom";
import Container from "@mui/material/Container";
import {CardComponent} from "../../components";

export const MovieDetailScreen = () => {
  const {item} = useMovieDetail();
  const navigate = useNavigate()
  if(!item?.title){
    return <h1>Cargando</h1>
  }
  return (
      <Container style={{
        display:'flex'
      }}>
        <div>
          <h2>{item.title}</h2>
          <img style={{
            width:200,
          }} src={`http://localhost:4000/api/images/${item.image}`} alt={'character'}/>
          <h3>Rate:{item.rate}</h3>
          <h3>Fecha de creacion:{item.date}</h3>
        </div>
        <h3>Personajes asociadas</h3>
        <div style={{
          display:'flex',
          gap:20,
          marginTop:50,
        }}>
          {item.Characters.map((character)=>{
            return (
                <CardComponent title={character.name} description={character.history} image={`http://localhost:4000/api/images/${character.image}`} onClick={()=>navigate('/character/'+character.id)}/>
            )
          })}
        </div>


      </Container>
  )
}
