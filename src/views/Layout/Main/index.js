import {HeaderComponent, FooterComponent} from "../../components";
import {Outlet} from "react-router-dom";

export const MainLayout = ({children}) => {
  return (
      <div style={{
          backgroundColor:'#1B1D27',
          height:'100vh'
      }}>
          <HeaderComponent/>
          <div>
              <Outlet />
          </div>
          <FooterComponent/>
      </div>
  )
}
