import { configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import {handleReducer} from "../../models/reducer";

const reducer = handleReducer();
export const persistReduce =  persistReducer(
    {
        key:"root",
        storage,
        whitelist:['auth']
    },
    reducer
)
export const store =  configureStore({
    reducer: persistReduce,
    preloadedState:{},
    devTools:process.env.NODE_ENV==='development',
    middleware:[thunk]
})

export const persist =   persistStore(store)




