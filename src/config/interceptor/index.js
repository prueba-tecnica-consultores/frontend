import axios from 'axios';
import {useEffect} from "react";
import Swal from "sweetalert2";

export const useInterceptor = (store) => {
    const handleRequestSuccess = (request)=>{
        const {auth} = store.getState()
        if(auth.token){
            request.headers.Authorization = `Bearer ${auth.token}`
        }
        return request
    }
    const handleRequestError = (error)=>{
        console.error('REQUEST ERROR',error)
    }
    const  handleResponseSuccess = (response)=>{
        const {data} = response
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 1500
        })
        return response
    }
    const handleResponseError = (error)=>{
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: error.response.data.msg,
            showConfirmButton: false,
            timer: 1500
        })
        return error
    }

    useEffect(()=>{
        axios.defaults.baseURL = `http://localhost:4000/api`
        axios.interceptors.request.use(handleRequestSuccess,handleRequestError)
        axios.interceptors.response.use(handleResponseSuccess,handleResponseError)
    },[])
}
