
import {ThemeProvider} from "@mui/material";
import {theme} from "./config";
import {
    RouterProvider,
} from "react-router-dom";
import {persist, store} from "./config/redux";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import {useInterceptor} from "./config";
import {router} from "./route";

function App() {
    useInterceptor(store);
  return (
      <ThemeProvider theme={theme} >
            <Provider store={store}>
                <PersistGate persistor={persist}>
                        <RouterProvider router={router}/>
                </PersistGate>
            </Provider>

      </ThemeProvider>
  );
}

export default App;
